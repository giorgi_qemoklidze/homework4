package com.example.homework4

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.isGone
import com.example.homework4.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.imgViewAndroidIcon.setOnClickListener{

            binding.imgViewGraveStone.setImageResource(R.drawable.grave_stone_2)
            binding.imgViewLollipop.setImageResource(R.drawable.lolipop_2)
            binding.imgViewCastle.setImageResource(R.drawable.castle_2)
            binding.imgViewPumpkin.setImageResource(R.drawable.pumpkin_4)
            binding.imgViewCoffin.setImageResource(R.drawable.coffin_2)
            binding.imgViewCandle.setImageResource(R.drawable.candle_2)
            binding.imgViewPumpkin2.setImageResource(R.drawable.pumpkin_3)
            binding.imgViewHat.setImageResource(R.drawable.hat_2)
            binding.root.setBackgroundColor(Color.rgb(57, 26,84 ))
            binding.imgViewCircle.setImageResource(R.drawable.white_circle_2)
            binding.txt.setText("")

        }

    }
}